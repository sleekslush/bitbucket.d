module bitbucket.api;

import bitbucket.endpoints.base;
import bitbucket.endpoints.v1.privileges;
import bitbucket.endpoints.v2.pullrequests;
import bitbucket.endpoints.v2.repositories;
import bitbucket.endpoints.v2.users;
import bitbucket.rest.client;
import bitbucket.rest.response;

class Bitbucket
{
    private RestClient _restClient;
    private PrivilegesEndpoint _privileges;
    private PullRequestsEndpoint _pullRequests;
    private RepositoriesEndpoint _repos;
    private UsersEndpoint _users;
    private string _username;

    this()
    {
        this(new RestClient);
    }
    
    package this(RestClient restClient)
    {
        _restClient = restClient;
        _privileges = new PrivilegesEndpoint(this);
        _pullRequests = new PullRequestsEndpoint(this);
        _repos = new RepositoriesEndpoint(this);
        _users = new UsersEndpoint(this);
    }

    this(in string username, in string password)
    {
        this();
        setAuthentication(username, password);
    }

    void setAuthentication(in string username, in string password)
    {
        _restClient.setAuthentication(username, password);
        _username = username;
    }

    @property string username()
    {
        return _username;
    }
    
    @property PrivilegesEndpoint privileges()
    {
        return _privileges;
    }

    @property PullRequestsEndpoint pullRequests()
    {
        return _pullRequests;
    }

    @property RepositoriesEndpoint repos()
    {
        return _repos;
    }

    @property UsersEndpoint users()
    {
        return _users;
    }

    package Response get(in char[] resourceURL, in string[string] queryParts = null)
    {
        auto response = _restClient.get(resourceURL, queryParts);
        return Response(response);
    }

    package void del(in char[] resourceURL)
    {
        _restClient.del(resourceURL);
    }
}

version(unittest)
{
    package class MockBitbucket : Bitbucket
    {
    }
}

unittest
{
    auto bb = new MockBitbucket;
}
