module bitbucket.endpoints.base;

import bitbucket.api;
import bitbucket.resources.base;
import bitbucket.rest.response;

abstract class Endpoint
{
    protected Bitbucket _api;

    this(Bitbucket api)
    {
        _api = api;
    }

    @property Bitbucket api()
    {
        return _api;
    }

    @property string baseURL();

    protected Response getResponse(in char[] resourcePath, string[string] queryParts = null)
    {
        auto url = baseURL ~ resourcePath;
        return _api.get(url, queryParts);
    }

    protected R getResource(R : Resource)(in char[] resourcePath, string[string] queryParts = null)
    {
        auto response = getResponse(resourcePath, queryParts);
        return new R(_api, response.json);
    }

    protected void delResource(in char[] resourcePath)
    {
        _api.del(baseURL ~ resourcePath);
    }
}

template EndpointCtor()
{
    this(Bitbucket api)
    {
        super(api);
    }
}
