module bitbucket.endpoints.v1.privileges;

import bitbucket.api;
import bitbucket.endpoints.base;
import bitbucket.endpoints.v1.base;

class PrivilegesEndpoint : EndpointV1
{
    mixin EndpointCtor;

    void deleteFrom(in char[] accountName, in char[] repoSlug, in char[] privilegeAccount)
    {
        delResource("/privileges/" ~ accountName ~ "/" ~ repoSlug ~ "/" ~ privilegeAccount);
    }
}
