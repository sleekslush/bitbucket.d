module bitbucket.endpoints.v1.base;

import bitbucket.api;
import bitbucket.endpoints.base;

abstract class EndpointV1 : Endpoint
{
    mixin EndpointCtor;

    override @property string baseURL()
    {
        return "https://bitbucket.org/api/1.0";
    }
}
