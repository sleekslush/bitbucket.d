module bitbucket.endpoints.v2.repositories;

import bitbucket.api;
import bitbucket.endpoints.base;
import bitbucket.endpoints.v2.base;
import bitbucket.resources.v2.commit;
import bitbucket.resources.v2.repository;
import bitbucket.resources.v2.user;

class RepositoriesEndpoint : EndpointV2
{
    mixin EndpointCtor;

    RepositoryList all(in char[] owner)
    {
        return getResource!RepositoryList(basePath(owner));
    }

    Repository get(in char[] repoName)
    {
        return getResource!Repository(basePath(repoName));
    }

    RepositoryList forks(in char[] repoName)
    {
        return getResource!RepositoryList(basePath(repoName) ~ "/forks");
    }

    BasicUserList watchers(in char[] repoName)
    {
        return getResource!BasicUserList(basePath(repoName) ~ "/watchers");
    }

    CommitList commits(in char[] repoName)
    {
        return getResource!CommitList(commitsPath(repoName));
    }

    FullCommit commit(in char[] repoName, in char[] revision)
    {
        return getResource!FullCommit(commitPath(repoName) ~ "/" ~ revision);
    }

    string diff(in char[] repoName, in char[] spec)
    {
        auto response = getResponse(basePath(repoName) ~ "/diff/" ~ spec);
        return response.toString();
    }

    string patch(in char[] repoName, in char[] spec)
    {
        auto response = getResponse(basePath(repoName) ~ "/patch/" ~ spec);
        return response.toString();
    }

    private char[] basePath(in char[] ownerOrRepoName)
    {
        return "/repositories/" ~ ownerOrRepoName;
    }

    private char[] commitsPath(in char[] repoName)
    {
        return basePath(repoName) ~ "/commits";
    }

    private char[] commitPath(in char[] repoName)
    {
        return basePath(repoName) ~ "/commit";
    }
}
