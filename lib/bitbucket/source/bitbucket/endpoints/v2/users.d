module bitbucket.endpoints.v2.users;

import bitbucket.api;
import bitbucket.endpoints.base;
import bitbucket.endpoints.v2.base;
import bitbucket.resources.v2.user;

class UsersEndpoint : EndpointV2
{
    mixin EndpointCtor;

    User get(in char[] username)
    {
        return getResource!User(basePath(username));
    }

    UserList followers(in char[] username)
    {
        return getResource!UserList(basePath(username) ~ "/followers");
    }

    UserList following(in char[] username)
    {
        return getResource!UserList(basePath(username) ~ "/following");
    }

    private char[] basePath(in char[] username)
    {
        return "/users/" ~ username;
    }
}
