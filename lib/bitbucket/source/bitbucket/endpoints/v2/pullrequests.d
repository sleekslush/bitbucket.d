module bitbucket.endpoints.v2.pullrequests;

import bitbucket.api;
import bitbucket.endpoints.base;
import bitbucket.endpoints.v2.base;
import bitbucket.resources.v2.commit;
import bitbucket.resources.v2.pullrequest;
import std.conv;

enum PullRequestState
{
    OPEN = "OPEN",
    MERGED = "MERGED",
    DECLINED = "DECLINED"
}

class PullRequestsEndpoint : EndpointV2
{
    mixin EndpointCtor;

    BasicPullRequestList all(in char[] repoName, PullRequestState state = null)
    {
        return (state is null)
            ? getResource!BasicPullRequestList(basePath(repoName))
            : getResource!BasicPullRequestList(basePath(repoName), ["state": state]);
    }

    PullRequest get(in char[] repoName, ulong id)
    {
        return getResource!PullRequest(basePath(repoName, id));
    }

    CommitList commits(in char[] repoName, ulong id)
    {
        return getResource!CommitList(basePath(repoName, id) ~ "/commits");
    }

    private char[] basePath(in char[] repoName)
    {
        return "/repositories/" ~ repoName ~ "/pullrequests";
    }

    private char[] basePath(in char[] repoName, ulong id)
    {
        auto path = basePath(repoName);
        return path ~ "/" ~ id.to!string;
    }
}
