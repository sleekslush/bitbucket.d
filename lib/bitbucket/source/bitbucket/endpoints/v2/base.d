module bitbucket.endpoints.v2.base;

import bitbucket.api;
import bitbucket.endpoints.base;

abstract class EndpointV2 : Endpoint
{
    mixin EndpointCtor;

    override @property string baseURL()
    {
        return "https://bitbucket.org/api/2.0";
    }
}
