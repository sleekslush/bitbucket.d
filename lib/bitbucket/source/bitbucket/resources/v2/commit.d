module bitbucket.resources.v2.commit;

import bitbucket.api;
import bitbucket.resources.base;
import bitbucket.resources.v2.repository;
import bitbucket.resources.v2.user;
import std.datetime;
import std.json;

alias ResourceList!BasicCommit BasicCommitList;
alias ResourceList!Commit CommitList;
alias ResourceList!Participant ParticipantList;

class BasicCommit : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string hash()
    {
        return json["hash"].str;
    }
}

class Commit : BasicCommit
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property Author author()
    {
        auto author = json["author"];
        return new Author(api, author);
    }

    @property BasicRepository repo()
    {
        auto repo = json["repository"];
        return new BasicRepository(api, repo);
    }

    @property SysTime date()
    {
        auto date = json["date"].str;
        return SysTime.fromISOExtString(date);
    }

    @property string message()
    {
        return json["message"].str;
    }

    @property BasicCommitList parents()
    {
        auto parents = json["parents"];
        return new BasicCommitList(api, parents);
    }
}

class FullCommit : Commit
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property ParticipantList participants()
    {
        auto participants = json["participants"];
        return new ParticipantList(api, participants);
    }
}

class Author : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string raw()
    {
        return json["raw"].str;
    }

    @property BasicUser user()
    {
        auto user = json["user"];
        return new BasicUser(api, user);
    }
}

class Participant : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string role()
    {
        return json["role"].str;
    }

    @property BasicUser user()
    {
        auto user = json["user"];
        return new BasicUser(api, user);
    }

    @property bool approved()
    {
        return json["approved"].type == JSON_TYPE.TRUE;
    }
}
