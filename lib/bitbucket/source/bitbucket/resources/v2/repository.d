module bitbucket.resources.v2.repository;

import bitbucket.api;
import bitbucket.resources.base;
import bitbucket.resources.v2.pullrequest;
import bitbucket.resources.v2.repository;
import bitbucket.resources.v2.user;
import std.datetime;
import std.json;

alias ResourceList!Repository RepositoryList;

class BasicRepository : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string fullName()
    {
        return json["full_name"].str;
    }

    @property string name()
    {
        return json["name"].str;
    }
}

class Repository : BasicRepository
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string description()
    {
        return json["description"].str;
    }

    @property string scm()
    {
        return json["scm"].str;
    }

    @property string hasWiki()
    {
        return json["has_wiki"].str;
    }

    @property SysTime updatedOn()
    {
        auto updatedOn = json["updated_on"].str;
        return SysTime.fromISOExtString(updatedOn);
    }

    @property string forkPolicy()
    {
        return json["fork_policy"].str;
    }

    @property SysTime createdOn()
    {
        auto createdOn = json["created_on"].str;
        return SysTime.fromISOExtString(createdOn);
    }

    @property BasicUser owner()
    {
        auto owner = json["owner"];
        return new BasicUser(api, owner);
    }

    @property ulong size()
    {
        return json["size"].uinteger;
    }

    @property BasicRepository parent()
    {
        auto parent = "parent" in json.object;

        return parent
            ? new BasicRepository(api, *parent)
            : null;
    }

    @property bool hasIssues()
    {
        return json["has_issues"].type == JSON_TYPE.TRUE;
    }

    @property bool isPrivate()
    {
        return json["is_private"].type == JSON_TYPE.TRUE;
    }

    @property string language()
    {
        return json["language"].str;
    }

    @property BasicPullRequestList pullRequests()
    {
        return api.pullRequests.all(fullName);
    }

    @property PullRequest pullRequest(ulong id)
    {
        return api.pullRequests.get(fullName, id);
    }

    @property RepositoryList forks()
    {
        return api.repos.forks(fullName);
    }

    @property BasicUserList watchers()
    {
        return api.repos.watchers(fullName);
    }
}
