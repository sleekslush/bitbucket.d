module bitbucket.resources.v2.pullrequest;

import bitbucket.api;
import bitbucket.resources.base;
import bitbucket.resources.v2.user;
import std.datetime;
import std.json;

alias ResourceList!BasicPullRequest BasicPullRequestList;
alias ResourceList!Participant ParticipantList;

class BasicPullRequest : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string state()
    {
        return json["state"].str;
    }

    @property string description()
    {
        return json["description"].str;
    }

    @property string title()
    {
        return json["title"].str;
    }

    @property bool closeSourceBranch()
    {
        return json["close_source_branch"].type == JSON_TYPE.TRUE;
    }

    /*@property CBR destination()
    {
    }*/

    @property string reason()
    {
        return json["reason"].str;
    }

    @property ulong id()
    {
        return json["id"].uinteger;
    }

    /*@property CBR source()
    {
    }*/

    @property SysTime createdOn()
    {
        auto createdOn = json["created_on"].str;
        return SysTime.fromISOExtString(createdOn);
    }

    @property BasicUser author()
    {
        auto author = json["author"];
        return new BasicUser(api, author);
    }

    @property SysTime updatedOn()
    {
        auto updatedOn = json["updated_on"].str;
        return SysTime.fromISOExtString(updatedOn);
    }

    /*@property Commit mergeCommit()
    {
    }*/

    @property BasicUser closedBy()
    {
        auto closedBy = json["closed_by"];
        return (closedBy.type == JSON_TYPE.NULL)
            ? null
            : new BasicUser(api, closedBy);
    }

    /*@property ActivityList activity()
    {

    }*/
}

class PullRequest : BasicPullRequest
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property BasicUserList reviewers()
    {
        auto reviewers = json["reviewers"];
        return new BasicUserList(api, reviewers);
    }

    @property ParticipantList participants()
    {
        auto participants = json["participants"];
        return new ParticipantList(api, participants);
    }
}

class Participant : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string role()
    {
        return json["role"].str;
    }

    @property BasicUser user()
    {
        auto user = json["user"];
        return new BasicUser(api, user);
    }

    @property bool approved()
    {
        return json["approved"].type == JSON_TYPE.TRUE;
    }
}
