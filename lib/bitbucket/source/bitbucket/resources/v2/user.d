module bitbucket.resources.v2.user;

import bitbucket.api;
import bitbucket.resources.base;
import bitbucket.resources.v2.repository;
import std.datetime;
import std.json;

alias ResourceList!BasicUser BasicUserList;
alias ResourceList!User UserList;

class BasicUser : ResourceV2
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property string username()
    {
        return json["username"].str;
    }

    @property string displayName()
    {
        return json["display_name"].str;
    }

    @property UserList followers()
    {
        return api.users.followers(username);
    }

    @property UserList following()
    {
        return api.users.following(username);
    }

    @property RepositoryList repos()
    {
        return api.repos.all(username);
    }
}

class User : BasicUser
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }

    @property SysTime createdOn()
    {
        auto createdOn = json["created_on"].str;
        return SysTime.fromISOExtString(createdOn);
    }

    @property string location()
    {
        return json["location"].str;
    }

    @property string type()
    {
        return json["type"].str;
    }

    @property string website()
    {
        return json["website"].str;
    }
}

