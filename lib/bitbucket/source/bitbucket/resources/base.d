module bitbucket.resources.base;

import bitbucket.api;
import std.json;

abstract class Resource
{
    protected Bitbucket api;
    protected JSONValue json;

    this(Bitbucket api, JSONValue json)
    {
        this.api = api;
        this.json = json;
    }
}

abstract class ResourceV2 : Resource
{
    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
    }
}

class ResourceList(R) : ResourceV2
{
    protected JSONValue[] _values;

    this(Bitbucket api, JSONValue json)
    {
        super(api, json);
        _values = json["values"].array;
    }

    @property ResourceList!R nextPage()
    {
        return paginate("next");
    }

    @property ResourceList!R prevPage()
    {
        return paginate("previous");
    }

    @property ulong length()
    {
        return _values.length;
    }

    int opApply(int delegate(R) dg)
    {
        auto result = 0;

        foreach (ref value; _values)
        {
            result = dg(new R(api, value));
            if (result)
            {
                break;
            }
        }

        return result;
    }

    private ResourceList!R paginate(in char[] pageName)
    {
        auto page = pageName in json.object;
        if (!page)
        {
            return null;
        }

        auto pageURL = (*page).str;
        auto response = api.get(pageURL);

        return new ResourceList!R(api, response.json);
    }
}
