module bitbucket.rest.client;

import std.algorithm;
import std.array;
import std.json;
import std.net.curl;
import std.uri;

class RestClient
{
    protected string _username;
    protected string _password;

    @property HTTP conn()
    {
        auto conn = HTTP();
        if (_username !is null || _password !is null)
        {
            conn.setAuthentication(_username, _password);
        }
        return conn;
    }

    void setAuthentication(in string username, in string password)
    {
        _username = username;
        _password = password;
    }

    char[] get(in char[] url, in string[string] queryParams=null)
    {
        return .get(addQueryParams(url, queryParams), conn);
    }

    char[] post(in char[] url, in string[string] postData)
    {
        auto payload = formEncode(postData);
        return .post(url, payload, conn);
    }

    char[] put(in char[] url, in string[string] putData)
    {
        auto _conn = conn;
        _conn.addRequestHeader("Accept", "application/json");
        _conn.addRequestHeader("Content-Type", "application/json");

        auto payload = buildJSONPayload(putData);
        return .put(url, payload, _conn);
    }

    void del(in char[] url)
    {
        .del(url, conn);
    }
}

private string formEncode(in string[string] queryParams)
{
    string[] buffer;

    foreach (key, ref value; queryParams)
    {
        buffer ~= key ~ "=" ~ value.encodeComponent();
    }

    return buffer.join("&");
}

unittest
{
    assert(formEncode(["a": "b"]) == "a=b");
    assert(formEncode(["a": "b", "c": "d"]) == "a=b&c=d");
    assert(formEncode(["a": "m&m"]) == "a=m%26m");
    assert(formEncode(["a": "m&m", "b": "e=mc2"]) == "a=m%26m&b=e%3Dmc2");
}

private const(char[]) addQueryParams(in char[] url, in string[string] queryParams=null)
{
    return (queryParams is null)
        ? url
        : url ~ "?" ~ formEncode(queryParams);
}

unittest
{
    auto url = "http://www.test.com";
    assert(addQueryParams(url) == url);
    assert(addQueryParams(url, ["a": "b"]) == url ~ "?a=b");
    assert(addQueryParams(url, ["a": "b", "c": "d"]) == url ~ "?a=b&c=d");
}

private string buildJSONPayload(in string[string] data)
{
    JSONValue json;
    json.object = data.toJSONObject();
    return (&json).toJSON();
}

private JSONValue[string] toJSONObject(in string[string] map)
{
    JSONValue[string] object;

    foreach (key, value; map)
    {
        object[key] = JSONValue(value);
    }

    return object;
}
