module bitbucket.rest.response;

import std.json;

struct Response {
    private const(char)[] _response;
    private JSONValue _json = null;

    this(const(char)[] response) {
        _response = response;
    }

    @property JSONValue json()
    {
        if (_json.type == JSON_TYPE.NULL)
        {
            _json = _response.parseJSON();
        }

        return _json;
    }

    string toString() {
        return cast(string) _response;
    }
}
