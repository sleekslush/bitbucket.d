import bb.auth;
import bb.cli;
import bitbucket.api;

int main(string[] args)
{
    auto bitbucket = new Bitbucket;
    authenticate(bitbucket);
    return dispatchCommand(bitbucket, args);
}
