module bb.commands.commits;

import bitbucket.api;
import bb.commands.base;
import std.exception;
import std.stdio;
import std.string;

class CommitsCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        enforce(args.length > 1, "You must specify a repository");

        auto repoName = args[1];
        auto commits = bitbucket.repos.commits(repoName);

        foreach (commit; commits)
        {
            writefln("%s\t%s\t%s", commit.date, commit.hash, commit.message.stripRight());
        }
    }
}
