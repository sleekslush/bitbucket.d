module bb.commands.repos;

import bitbucket.api;
import bb.commands.base;
import std.stdio;

class ReposCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        auto repos = bitbucket.repos.all(bitbucket.username);

        foreach (repo; repos)
        {
            writefln("%s (%s)", repo.name, repo.fullName);
        }
    }
}
