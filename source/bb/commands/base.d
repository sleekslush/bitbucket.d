module bb.commands.base;

import bitbucket.api;
import std.string;

/**
  Represents a command that we can execute.
  */
interface Command
{
    /**
      Execute some arbitrary set of statements.

        Params:
            args = An array of arguments from the command-line
      */
    void execute(Bitbucket bitbucket, string[] args);
}

/**
  Finds and instantiates a derived Command class that matches the provided command name.

    Params:
        commandName = The name of the command to find

    Returns:
        An derived instance of Command if the command was found. If no command was found
        then null is returned.
  */
Command getCommand(string commandName)
{
    auto fullClassName = getFullClassName(commandName);
    return cast(Command) Object.factory(fullClassName);
}

/**
  Constructs the full class name intended to be instantiated using Object.factory.

    Params:
        commandName = The command to construct a full class name for

    Returns:
        A full class name.

    Examples:
        auto fullClassName = getFullClassName("clone");
        assert(fullClassName == "bb.commands.clone.CloneCommand");
  */
private string getFullClassName(string commandName)
{
    auto className = commandName[0..1].toUpper() ~ commandName[1..$];
    return "bb.commands.%s.%sCommand".format(commandName, className);
}

unittest
{
    auto commandName = "clone";

    auto fullClassName = getFullClassName(commandName);
    assert(fullClassName == "bb.commands.clone.CloneCommand", "Full command class path invalid");

    auto invalidCommand = getCommand("invalid");
    assert(invalidCommand is null, "Invalid command did not return null");

    auto command = getCommand(commandName);
    assert(is(typeof(command) == Command), "Valid command did not get instantiated properly");

    import bb.commands.clone;
    assert(is(typeof(cast(CloneCommand) command) == CloneCommand), "Unable to downcast command");
}
