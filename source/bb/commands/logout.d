module bb.commands.logout;

import bb.commands.base;
import bb.config;
import bitbucket.api;

class LogoutCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        removeConfig("username", false);
        removeConfig("password");
    }
}
