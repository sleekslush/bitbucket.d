module bb.commands.diff;

import bitbucket.api;
import bb.commands.base;
import std.exception;
import std.stdio;
import std.string;

class DiffCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        enforce(args.length > 2, "You must specify a repository and a spec");

        auto repoName = args[1];
        auto spec = args[2];

        auto diff = bitbucket.repos.diff(repoName, spec);
        writeln(diff);
    }
}
