module bb.commands.login;

import bb.commands.base;
import bb.config;
import bitbucket.api;
import std.getopt;
import std.stdio;

class LoginCommand : Command
{
    private string username;
    private string password;

    override void execute(Bitbucket bitbucket, string[] args)
    {
        parseOptions(args);

        if (username && password)
        {
            setConfig("username", username, false);
            setConfig("password", password); // flush
        }
        else
        {
            writeln("You need to specify both a username and a password.");
        }
    }

    void parseOptions(string[] args)
    {
        getopt(args,
            "username|u", &username,
            "password|p", &password
        );
    }
}
