module bb.commands.privileges;

import bitbucket.api;
import bb.commands.base;

class PrivilegesCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        bitbucket.privileges.deleteFrom(args[0], args[1], args[2]);
    }
}
