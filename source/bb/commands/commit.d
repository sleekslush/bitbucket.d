module bb.commands.commit;

import bitbucket.api;
import bb.commands.base;
import std.exception;
import std.stdio;
import std.string;

class CommitCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        enforce(args.length > 2, "You must specify a repository and a commit hash");

        auto repoName = args[1];
        auto commitHash = args[2];

        auto commit = bitbucket.repos.commit(repoName, commitHash);
        writefln("%s\t%s\t%s", commit.date, commit.hash, commit.message.stripRight());
    }
}
