module bb.commands.help;

import bitbucket.api;
import bb.commands.base;
import std.stdio;

class HelpCommand : Command
{
    override void execute(Bitbucket bitbucket, string[] args)
    {
        writefln("Usage: %s <command> [args]", args[0]);
    }
}
