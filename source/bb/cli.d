module bb.cli;

import bitbucket.api;
import bb.commands.base;
import bb.config;
import std.exception;
import std.stdio;

int dispatchCommand(Bitbucket bitbucket, string[] args)
{
    auto commandName = (args.length == 1) ? "help" : args[1];
    auto command = getCommand(commandName);
    enforce(command, "Invalid command: " ~ commandName);
    auto extraArgsIndex = (args.length == 1) ? 1 : 2;

    try
    {
        command.execute(bitbucket, args[0] ~ args[extraArgsIndex..$]);
        return 0;
    }
    catch (Exception ex)
    {
        writeln(ex.msg);
        debug writeln(ex.info);
        return 1;
    }
}
