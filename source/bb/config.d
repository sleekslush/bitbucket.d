module bb.config;

import std.ascii;
import std.file;
import std.json;
import std.path;

JSONValue config;

static this()
{
    if (configFile.exists)
    {
        auto content = cast(string) read(configFile);
        config = content.parseJSON();
        assert(config.type == JSON_TYPE.OBJECT, "Corrupt config file!");
    }
    else
    {
        JSONValue[string] obj;
        config.object = obj;
    }
}

@property string configDir()
{
    return "~/.config/bb".expandTilde();
}

@property string configFile()
{
    return buildPath(configDir, "defaults");
}

void setConfig(T)(string key, T value, bool autoFlush = true)
{
    static if (is(T == JSONValue))
    {
        config.object[key] = value;
    }
    else
    {
        config.object[key] = JSONValue(value);
    }

    if (autoFlush)
    {
        flush();
    }
}

T getConfig(T)(string key)
{
    auto value = key in config.object;
    if (!value)
    {
        return null;
    }

    static if (is(T == string))
    {
        return value.str;
    }
    else if (is(T == ulong))
    {
        return value.integer;
    }
    else if (is(T == long))
    {
        return value.uinteger;
    }
    else if (is(T == real))
    {
        return value.floating;
    }
    else if (is(T == JSONValue[string]))
    {
        return value.object;
    }
    else if (is(T == JSONValue[]))
    {
        return value.array;
    }
    else
    {
        static assert("Unknown return type for JSON object lookup");
    }
}

void removeConfig(string key, bool autoFlush = true)
{
    config.object.remove(key);

    if (autoFlush)
    {
        flush();
    }
}

void flush()
{
    if (!configDir.exists)
    {
        mkdirRecurse(configDir);
    }

    auto content = (&config).toJSON(true);
    write(configFile, content ~ newline);
}
