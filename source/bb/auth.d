module bb.auth;

import bb.config;
import bitbucket.api;

void authenticate(Bitbucket bitbucket)
{
    auto username = getConfig!(string)("username");
    auto password = getConfig!(string)("password");

    if (username && password)
    {
        bitbucket.setAuthentication(username, password);
    }
}
